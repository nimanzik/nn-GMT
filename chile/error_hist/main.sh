#!/bin/bash

gmtset FONT_TITLE 20p,31 
gmtset FONT_ANNOT_PRIMARY 16p,31                                                   
gmtset FONT_LABEL 20p,31

ps=UNCERT_HIST_LAND.ps

SINGLE=$1
SSST=$2

J=X3i/2.5i

Z=1

Li=thick,GRAY30
Gi=DARKGRAY

Lf=thick,BLUE4

YLABEL="Frequency"

FONT=+f16p,29+jML


# ---------------------- DEPTH UNCERTAINTIES ---------------------------------
Rz=0/80/0/25
Wz=4
XTICKz=a20f4
XLABELz='Depth Uncert. [km]'

awk 'NR>1{print $3}' $SINGLE | pshistogram -R$Rz -J$J -W$Wz -Z$Z -L$Li -G$Gi \
    -Bpya5+l$YLABEL+u"%" -Bpx${XTICKz}+l"$XLABELz" -BeWnS -X2.4 -Y5 -K > $ps
awk 'NR>1{print $3}' $SSST | pshistogram -R -J -W$Wz -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
30 23.5
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
30 20.5
EOF

pstext -R -J -F$FONT -K -O >> $ps << EOF
34 23.5 Single event loc.
34 20.5 SSST locations
EOF


# ---------------------- MAX HORIZONTAL UNCERTAINTIES ------------------------
Rh=0/100/0/25
Wh=5
XTICKh=a20f5
XLABELh='Horizontal Uncert. [km]'

awk 'NR>1{print $4}' $SINGLE | pshistogram -R$Rh -J$J -W$Wh -Z$Z -L$Li -G$Gi \
    -Bpya5+u"%" -Bpx${XTICKh}+l"$XLABELh" -BeWnS -X3.7i -O -K >> $ps
awk 'NR>1{print $4}' $SSST | pshistogram -R -J -W$Wh -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
40 23.5
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
40 20.5
EOF

pstext -R -J -F$FONT -K -O >> $ps << EOF
44 23.5 Single event loc.
44 20.5 SSST locations
EOF


# ---------------------- RESIDUAL RMS-----------------------------------------
Rr=0/3/0/25
Wr=0.15
XTICKr=a0.6f0.15
XLABELr="Travel-time residual RMS [s]"

awk 'NR>1{print $5}' $SINGLE | pshistogram -R$Rr -J$J -W$Wr -Z$Z -L$Li -G$Gi \
    -Bpya5+u"%" -Bpx${XTICKr}+l"$XLABELr" -BeWnS -X3.7i -O -K >> $ps
awk 'NR>1{print $5}' $SSST | pshistogram -R -J -W$Wr -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
1.2 23.5
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
1.2 20.5
EOF

pstext -R -J -F$FONT -O >> $ps << EOF
1.35 23.5 Single event loc.
1.35 20.5 SSST locations
EOF


# --- Convert ps file
ps2raster $ps -TG -E600 -P -A0.2

