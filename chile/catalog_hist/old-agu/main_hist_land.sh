#!/bin/bash

gmtset FONT_TITLE 20p,31
gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31
gmtset MAP_TITLE_OFFSET 8p

# ############################################################################
# Input residual files (columns=residual[s], distance[deg], azimuth[deg]) 
SINGLE=$1
SSST=$2

ps=RESIDUALS.ps

# ############################################################################
# --- Histogram Config ---
Rh=-5/5/0/10
Jh=X3i/2.5i

Wh=0.1
Zh=1
Gh=DARKGOLDENROD3
Lh=thinnest,BLACK

YTICK=a2
YLABEL="Frequency"
XTICK=a2f1
XLABEL="Travel-time residual [s]"

FONT=+f16p,29,BLUE4+jTR
XT=4.5
YT=9.5


# ----------------------------------------------------------------------------
# histogram of initial residuals
awk 'NR>1{print $1}' $SINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+l"$YLABEL"+u"%" -Bpx$XTICK+l"$XLABEL" -BeWnS+t"Single event locations" -X2.5 -Y5 -K > $ps

# add text
echo "$XT $YT  N=`awk 'NR==1 {print $3}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-1.2 | bc -l`  MAD=`awk 'NR==1 {print $5}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2.4 | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3.6 | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of final residuals
awk 'NR>1{print $1}' $SSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+u"%" -Bpx$XTICK+l"$XLABEL" -BeWnS+t"SSST locations" -X3.7i -O -K >> $ps

# add text
echo "$XT $YT    N=`awk 'NR==1 {print $3}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-1.2 | bc -l`  MAD=`awk 'NR==1 {print $5}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2.4 | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3.6 | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SSST`" | pstext -R -J -F$FONT -N -O >> $ps


# --- Convert ps file ---
ps2raster $ps -TG -E600 -P -A0.2

