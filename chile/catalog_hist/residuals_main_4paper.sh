#!/bin/bash

gmtset PS_MEDIA 15.6cx12.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

RESFILE0=../results-16048_183409/single-residuals-vs-distance.dat
STATFILE0=../results-16048_183409/single-binned-stat-2deg.dat
TITLE0="Single-Event Locations"
RESFILE1=../results-16048_183409/ssst-residuals-vs-distance.dat
STATFILE1=../results-16048_183409/ssst-binned-stat-2deg.dat
TITLE1="Global SSST Locations"


ps=CHILE-RESIDUALS.ps


# #############################################################################
#           RESIDUAL HISTOGRAMS >>> CONFIG                                    #
# #############################################################################

RHIST=-8/8/0/12
J=X4.5c/4c

XTICK=a4f1
YTICK=a4f2

Wh=0.1
Zh=1
Gh=gray30
Lh=thin,gray30

X0T=-7.5
Y0T=11.3
DY=1.4
F=+f8.5p,Helvetica,BLACK+jTL


# #############################################################################
#           RESIDUAL HISTOGRAMS >>> PLOT                                      #
# #############################################################################

# --- Initialize ---
psxy -R$RHIST -J$J -T -X0 -Y0 -K > $ps


XOFFSET=(2.8c 5.5c)
YOFFSET=(7c 0)

for i in 0 1;do
    RESFILE=`eval "echo $"RESFILE$i""`
    TITLE=`eval "echo $"TITLE$i""`

    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}
    
    if [ $i = 0 ];then
        By=$YTICK+l"Frequency [%]"
        ANNOT=eWnS
    else
        By=$YTICK
        ANNOT=ewnS
    fi

    # Histogram
    awk 'NR>1 {print $2}' $RESFILE | pshistogram -R$RHIST -J$J -W$Wh -Z$Zh -G$Gh -L$Lh \
        -Bpx$XTICK+l"Travel Time Residual [s]" -Bpy"$By" -B$ANNOT+t"$TITLE" -X$X -Y$Y -O -K >> $ps

    # Add statistical measures text
    echo "$X0T `echo $Y0T | bc -l`  N=`awk 'NR==1 {print $3}' $RESFILE`" | \
        pstext -R -J -F$F -O -K >> $ps
    echo "$X0T `echo $Y0T-1*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-2*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-3*$DY | bc -l`  @%12%s@%%=$(echo "scale=2; sqrt(`awk 'NR==1 {print $9}' $RESFILE`)" | bc -l)" | \
        pstext -R -J -F$F -N -O -K >> $ps

done


# #############################################################################
#           RESIDUAL vs. DISTANCE >>> CONFIG                                  #
# #############################################################################

RDIST=0/100/-8/8

XTICK=a30f10
YTICK=a4f2

S=c0.003
G=GRAY10

WMEAN=1,RED
WSTD=1,BLACK,--


# #############################################################################
#           RESIDUAL vs. DISTANCE >>> PLOT                                    #
# #############################################################################

XOFFSET=(-5.5c 5.5c)
YOFFSET=(-5.8c 0)

for i in 0 1;do
    RESFILE=`eval "echo $"RESFILE$i""`
    STATFILE=`eval "echo $"STATFILE$i""`

    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}

    if [ $i = 0 ];then
        By=$YTICK+l"Travel Time Residual [s]"
        ANNOT=eWnS
    else
        By=$YTICK
        ANNOT=ewnS
    fi

    psbasemap -R$RDIST -J$J -Bpx$XTICK+l"Epicentral Distance [deg]" -Bpy"$By" -B$ANNOT -X$X -Y$Y -O -K >> $ps
    awk 'NR>1 {print $1, $2}' $RESFILE | psxy -R -J -S$S -G$G -O -K >> $ps
    awk 'NR>1 {print $1, $2}' $STATFILE | psxy -R -J -W$WMEAN -O -K >> $ps
    awk 'NR>1 {print $1, $3}' $STATFILE | psxy -R -J -W$WSTD  -O -K >> $ps
    awk 'NR>1 {print $1, -$3}' $STATFILE | psxy -R -J -W$WSTD -O -K >> $ps

done


# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-5.5c -Y4c -O -K >> $ps << END
-4 25.5 (a)
-4  2   (b)
END



# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert PS to PDF ---
psconvert $ps -Tf -P


rm gmt.conf gmt.history

