#!/bin/bash

gmtset PS_MEDIA 15.6cx7c
gmtset PS_PAGE_ORIENTATION portrait
gmtset MAP_ORIGIN_X 1i
gmtset MAP_ORIGIN_Y 1i
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica

ps=STATION-DIST.ps

META=./metadata
CHILE_P=$META/Pstations_noCX.lonlat
CHILE_pP=$META/pPstations_noCX.lonlat
TONGA_P=$META/tonga_Pstations.lonlat
TONGA_pP=$META/tonga_pPstations.lonlat

sSize=0.25
sCP=16/78/139 #GRAY35
sCpP=238/44/44 #0/0/169

DRY=150 #GRAY70 #DARKKHAKI
WET=white


# #############################################################################
#      Add the base-map                                                       #
# #############################################################################

psbasemap -Rd -JN-125/11c -BEWNs -X2.3c -Y0.7c -K > $ps
pscoast -R -J -Dc -G$DRY -S$WET -Wthinnest -Ba90g90/a45g45 -O -K >> $ps


# #############################################################################
#      Add seismic stations                                                   #
# #############################################################################
for i in $CHILE_pP $TONGA_pP;do
    awk '{print $2,$3}' $i | psxy -R -J -St$sSize -Wthinnest,0 -G$sCpP -O -K >> $ps
done

for i in $CHILE_P $TONGA_P;do
    awk '{print $2,$3}' $i | psxy -R -J -St$sSize -Wthinnest,0 -G$sCP -O -K >> $ps
done


# Add the station names
#awk '{print $2, $3-0.1, $2}' $sta | pstext -R -J -F+f6,darkgreen+jBL -O -K >> $ps


# #############################################################################
#      Add rectangles around the studied areas                                #
# #############################################################################

# Chile
psxy -R -J -W1.5,BLACK -A -K -O >> $ps << EOF
-80.5 -5.0
-62.0 -5.0
-62.0 -45.5
-80.5 -45.5
-80.5 -5
EOF


# Tonga-Fiji
psxy -R -J -W1.5,BLACK -A -K -O >> $ps << EOF
+170.0  -11.5
-171.0  -11.5
-171.0  -30.5
+170.0  -30.5
+170.0  -11.5
EOF


# #############################################################################
#      Add Text for rectangles around the studied areas                       #
# #############################################################################
pstext -R -J -F+f8p,Helvetica,BLACK+j -O -K >> $ps << EOF
-170.5  -32.5  TL  Tonga-Fiji region
-82.5  -5  TR  Chilean margin
EOF



# To add legend
#gmt pslegend -R0/9/0/0.5 -Jx2i -Dx2.7i/-0.2/3.0i/TC -Y-0.25i -F -O -K >> $ps << END
#N 2
#S 0.15i t 0.2i $sCpP  0.25p 0.3i P and pP picks
#S 0.65i t 0.2i $sCP   0.25p 0.8i P picks
#END

# Finalize
psxy -R -J -T -O >> $ps

# Conert Ps to PDF
psconvert $ps -Tf -P


rm gmt.conf gmt.history

