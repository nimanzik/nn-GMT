# Nsamples: 59  MAD:  0.490  mean: -0.031  variance:  1.569
gfz2010ahqi  -0.91180
gfz2010ebhc  -0.60490
gfz2010eruy   4.27930
gfz2010fync   2.83070
gfz2010nwef  -1.39350
gfz2010usho  -0.31870
gfz2011aatr  -0.38670
gfz2011dpqx  -0.52750
gfz2011enva  -0.57320
gfz2011gkzt  -1.36910
gfz2011hlqw   5.49170
gfz2011hlsa  -0.45780
gfz2011jplh  -0.43670
gfz2011lzwj   0.32960
gfz2011mdie  -0.12870
gfz2011owvz   1.38490
gfz2011rewa  -0.59950
gfz2011toub  -0.10170
gfz2011vhfi  -1.30230
gfz2011wxfs  -0.47990
gfz2011xyxj  -0.17730
gfz2012enno  -0.42540
gfz2012jlox  -0.48950
gfz2012kkcp  -0.12560
gfz2012ldxg  -0.12740
gfz2012ldzj  -0.75400
gfz2012sphd  -1.07390
gfz2012teic   0.62480
gfz2012ttoa   0.23600
gfz2012vrgb  -0.72100
gfz2012wypz  -0.18030
gfz2013drxz  -0.37150
gfz2013jkut   0.00830
gfz2013ldqb   2.31220
gfz2013nwdi  -0.77000
gfz2013pfbx  -1.21230
gfz2013svci  -0.02090
gfz2014fgvi   0.24730
gfz2014fhla  -0.44170
gfz2014ftka  -2.18980
gfz2014gkgf   0.14950
gfz2014gmge   1.78600
gfz2014gmhr   1.79350
gfz2014gmnb  -0.17770
gfz2014gull  -0.49210
gfz2014jjhc  -0.50880
gfz2014lgqn   0.10940
gfz2014lyhk  -0.80330
gfz2014nqfm  -0.56470
gfz2014nrie   0.27390
gfz2014oinz  -0.96160
gfz2014qlxd  -0.52730
gfz2014qqhm  -0.04610
gfz2014ssvz  -0.35140
gfz2014ubwn   0.13310
gfz2015fsjj  -0.53720
gfz2015gckd  -0.07610
gfz2015lhjy  -0.24920
gfz2015mpte   0.12330
