#!/bin/bash

gmtset FONT_TITLE 20p,31
gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31                                                        
gmtset MAP_TITLE_OFFSET 8p

SINGLE=$1

FNAME=$(basename "$SINGLE")
STA="${FNAME%.*.*}"

SSST=./res_ssst/$FNAME

ps=${STA}.ps

R=-8/8/0/20
J=X3i/2.5i

W=0.1
Z=1
G=DARKGRAY
L=thin,BLACK

FONT=+f16p,29,BLUE4+jTL
XT=-7
YT=19
DY=2.2

YTICK=a5
YLABEL='Frequency'
XTICK=a2f1
XLABEL="Travel-time residual [s]"


# ----------------------------------------------------------------------------
# histogram of initial residuals
awk 'NR>1{print $2}' $SINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+l$YLABEL+u"%" -Bpx$XTICK+l"$XLABEL" -BeWnS+t"NLLoc locations" -X2.5 -Y5 -K > $ps

# add text
echo "$XT $YT  N=`awk 'NR==1 {print $3}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3*$DY | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of final residuals
awk 'NR>1{print $2}' $SSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+u"%" -Bpx$XTICK+l"$XLABEL" -BeWnS+t"SSST locations" -X3.7i -K -O >> $ps

# add text
echo "$XT $YT  N=`awk 'NR==1 {print $3}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3*$DY | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SSST`" | pstext -R -J -F$FONT -N -O >> $ps


# convert to PNG
ps2raster $ps -TG -E600 -P -A0.2

mv *.png ./FIGS4AGU/pngFiles/
mv $ps ./FIGS4AGU/psFiles/

