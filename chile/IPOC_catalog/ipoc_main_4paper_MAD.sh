#!/bin/bash

gmtset PS_MEDIA 15.6cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

GTFILE=./GT/ipoc-selected-M4.nor
LOCFILE0=../results-16048_183409/single-locations.nor
TITLE0="Single-Event Locations"
ABBREV0="SE"
LOCFILE1=../results-16048_183409/ssst-locations.nor
TITLE1="Global SSST Locations"
ABBREV1="SSST"

SLAB=../results-16048_183409/slab1.0_sam_top.in
BOUNDFILE=./boundary.latlon
STAFILE=./cx_stations.latlon


ps=GT-IPOC.ps


# #############################################################################
#          HYPOCENTER MAPS >>> CONFIG                                         #
# #############################################################################

R1=-71/-68.01/-23.8/-17.8
J1=M3.8c
B1=a1f0.5

R2=-5/150/-23.5/-18
J2=X2c/8.1c

R3=-71/-68/-5/150
J3=X3.8c/-2c

# Merge Catalogs
T0=7
D0=200
T1=7
D1=50


# GT events
Sg=a0.2
Gg=238/44/44
Wg=thinnest,BLACK

# Relocated events
Sr0=c0.15
Sr1=d0.15
Wr=thinnest,BLACK

# Stations
Ss=t0.3
Gs=16/78/139
Ws=thin,BLACK
F=+f8.5p,Helvetica-Bold,BLACK+jTR

# Array line
Wa=thin,Black,--

# Offset lines
Wl=thinnest,BLACK

DRY=150 #GRAY70
WET=211 #235 #whitesmoke #LIGHTCYAN


# #############################################################################
#          HYPOCENTER MAPS >>> PLOT                                           #
# #############################################################################

# --- Initialize ---
psxy -R$R1 -J$J1 -T -X0 -Y0 -K > $ps


XOFFSET=(1.3c  8c)
YOFFSET=(13c  2.25c)


for i in 0 1;do
    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}
    Sr=`eval "echo $"Sr$i""`

    LOCFILE=`eval "echo $"LOCFILE$i""`
    T=`eval "echo $"T$i""`
    D=`eval "echo $"D$i""`

    # Merge Catalogs
    gmt=catmerge$i.gmt
    xyz=catmerge$i.xyz
    perl merge_seisan.pl -T=$T -D=$D -d=gmtd $GTFILE $LOCFILE > $gmt
    perl merge_seisan.pl -T=$T -D=$D -d=xyz  $GTFILE $LOCFILE > $xyz


    # ########## 1.: Lat-Lon Map ##########
    grdimage -R$R1 -J$J1 ../topo/chile.grd -I../topo/chile.norm  \
	    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
    pscoast -R$R1 -J$J1 -S$WET -Dh -Wthin -N1 -O -K >> $ps
    psbasemap -R -J -B$B1 -BeWNs -L-68.45/-23.35/-21/50 -O -K >> $ps

	# Slab
	awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+r+t -Gblack -O -K >> $ps

    # Array boundary
    awk '{print $3, $2}' $BOUNDFILE | psxy -R -J -W$Wa -A -O -K >> $ps

    # Stations
    awk '{print $3, $2}' $STAFILE | psxy -R -J -S$Ss -G$Gs -W$Ws -O -K >> $ps
    #awk '{print $3-0.06, $2+0.12, $1}' $STAFILE | pstext -R -J -F$F -O -K >> $ps

    # Offset Lines
    awk '{print $1, $2}' $gmt | psxy -R -J -W$Wl -A -O -K >> $ps

    # Hypocenters, GT
    awk '2 ==  NR % 3 {print $1, $2}' $gmt | psxy -R -J -S$Sg -G$Gg -W$Wg -O -K >> $ps

    # Hypocenters, Relocated
    awk '2 ==  (NR+2) % 3 {print $1, $2}' $gmt | psxy -R -J -S$Sr -W$Wr -O -K >> $ps


    # ########## 2.: Lat-Depth Map ##########
    psbasemap -R$R2 -J$J2 -Bxa50f25+l"Depth [km]" -By1f0.5+u"\\232" -BeNs -X3.95c -Y0 -O -K >> $ps
    psbasemap -R$R2 -J$J2 -By1000 -Bw -O -K >> $ps

    # Offset Lines
    awk '{print $3, $2, $1}' $gmt | awk '{print $1, $2'} | psxy -R -J -W$Wl -A -O -K >> $ps

    # Hypocenters, GT
    awk '2 ==  NR % 3 {print $3, $2}' $gmt | psxy -R -J -S$Sg -G$Gg -W$Wg -O -K >> $ps

    # Hypocenters, Relocated
    awk '2 ==  (NR+2) % 3 {print $3, $2}' $gmt | psxy -R -J -S$Sr -W$Wr -O -K >> $ps


    # ########## 3.: Lon-Depth Map ##########
    psbasemap -R$R3 -J$J3 -Bx1f0.5+u"\\232" -By50f25+l"Depth [km]" -BeWS -X-3.95c -Y-2.2c -O -K >> $ps
    psbasemap -R$R3 -J$J3 -Bx1000 -Bn -O -K >> $ps

    # Offset Lines
    awk '{print $1, $3}' $gmt | psxy -R -J -W$Wl -A -O -K >> $ps

    # Hypocenters; GT
    awk '2 ==  NR % 3 {print $1, $3}' $gmt | psxy -R -J -S$Sg -G$Gg -W$Wg -O -K >> $ps

    # Hypocenters; Relocated
    awk '2 ==  (NR+2) % 3 {print $1, $3}' $gmt | psxy -R -J -S$Sr -W$Wr -O -K >> $ps

done



# #############################################################################
#          LEGEND                                                             #
# #############################################################################

pslegend -R0/14/0/10 -JX1i -Dx0c/0c/14c/TL -L1.2 -C2p/2p -F+p0.1 -X-8c -Y-0.7c -O -K >> $ps << EOF
N 3
S 0.15i a 0.30 $Gg 0.2 0.3i IPOC Catalog Locations
S 0.15i c 0.25 WHITE 0.2 0.3i Single-Event Locations
S 0.15i d 0.25 WHITE 0.2 0.3i Global SSST Locations
EOF



# #############################################################################
#          OFFSET HISTOGRAMS >>> CONFIG                                       #
# #############################################################################

Rh=0/140/0/20
Rv=-70/70/0/20
J=X3.8c/2c

XTEXT=135
YTEXT=18.5
DY=3.1
F=+f8.5,Helvetica,BLACK+jTR

XTICK=a30f10
YTICK=a10f5

Wh=5
Zh=0
Gh=150 #DODGERBLUE4
Lh=thin,25 #deepskyblue4


# #############################################################################
#          OFFSET HISTOGRAMS >>> PLOT                                         #
# #############################################################################

# Convert .xyz Offsets to .hv Offsets (Horizontal & Vertical)
function horizontal_offset {
    h=$(echo "scale=2; sqrt($x^2 + $y^2)" | bc -l)
    echo $h
}

for i in 0 1;do
    awk '{print $1, $2, $3}' catmerge$i.xyz |
    while read x y z;do
        h=`horizontal_offset x y`
        echo "$h $z"
        done > catmerge$i.hv
done


function mean {
    count=0
    total=0
    for i in $ARRAY;do
        total=$(echo "$total + $i" | bc -l)
        ((count++))
    done

    mu=$(echo "scale=2; $total / $count" | bc -l)

    # bc removes decimal points and zero if the output is less then one (< 1)
    if (( $(bc <<< "$mu > 0") )) && (( $(bc <<< "$mu < 1") ));then
        echo "0$mu"
    elif (( $(bc <<< "$mu < 0") )) && (( $(bc <<< "$mu > -1") ));then
        IFS=- read var1 var2 <<< $mu
		echo "-0$var2"
    else
        echo $mu
    fi
}


function std {
    count=0
    sum=0
    for i in $ARRAY;do
        diff=$(echo "$mu - $i" | bc -l)
        diff2=$(echo "$diff * $diff" | bc -l)
        sum=$(echo "$sum + $diff2" | bc -l)
        ((count++))
    done

    div=$((count-1))
    sigma=$(echo "scale=2; sqrt($sum / $div)" | bc -l)

    # bc removes decimal points and zero if the output is less then one (< 1)
    if (( $(bc <<< "$sigma > 0") )) && (( $(bc <<< "$sigma < 1") ));then
        echo "0$sigma"
    elif (( $(bc <<< "$sigma < 0") )) && (( $(bc <<< "$sigma > -1") ));then
        IFS=- read var1 var2 <<< $sigma
        echo "-0$var2"
    else
        echo $sigma
    fi
}



function plot_histograms {

    for j in 0 1;do
	    X=${XORIGIN[$j]}
	    Y=${YORIGIN[$j]}
        mad=${MAD[$j]}

	    if [ $j = 0 ];then
	        R=$Rh
	        Bx=x$XTICK+l"Horizontal Offset [km]"
	        ANNOT=eWnS+t$TITLE
	    elif [ $j = 1 ];then
	        R=$Rv
	        Bx=$XTICK+l"Vertical Offset (Z@-$ABBREV@--Z@-IPOC@-) [km]"
	        ANNOT=eWnS
	    fi

        By=$YTICK+l"Counts"

	    awk -v k=$(($j+1)) '{print $k}' $OFFSETFILE | pshistogram -R$R -J$J -W$Wh -Z$Zh \
		    -G$Gh -L$Lh -Bx"$Bx" -By"$By" -B"$ANNOT" -X$X -Y$Y -O -K >> $ps
		
		ARRAY=$( awk -v k=$(($j+1)) '{print $k}' $OFFSETFILE )
		mu=`mean ARRAY`
		sigma=`std ARRAY mu`
        
        X=$XTEXT
        Y=$YTEXT
		if [ $j = 1 ];then
		    X=$(echo "$XTEXT - 70" | bc -l)
		fi
        echo "$X $Y N=49" | pstext -R -J -F$F -O -K >> $ps
        echo "$X `echo $Y-1*$DY | bc` MAD=$mad"     | pstext -R -J -F$F -O -K >> $ps
        echo "$X `echo $Y-2*$DY | bc` @~m@~=$mu"    | pstext -R -J -F$F -O -K >> $ps
		echo "$X `echo $Y-3*$DY | bc` @~s@~=$sigma" | pstext -R -J -F$F -O -K >> $ps
    done
}


XORIGIN=(2.15c  0c)
YORIGIN=(-3.5c -3.6c)
OFFSETFILE=catmerge0.hv
MAD=(10.39 16.90)
ABBREV=$ABBREV0
TITLE=$TITLE0
plot_histograms XORIGIN YORIGIN OFFSETFILE MAD TITLE ABBREV


XORIGIN=(5.85c  0c)
YORIGIN=(3.6c -3.6c)
OFFSETFILE=catmerge1.hv
MAD=(5.87 8.15)
ABBREV=$ABBREV1
TITLE=$TITLE1
plot_histograms XORIGIN YORIGIN OFFSETFILE MAD TITLE ABBREV



# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-8c -Y2c -O -K >> $ps << END
-3.5   71.5  (a)
-3.0   16.5  (b)
-3.0    1.0  (c)
END
#28.5 58 (b)
#28.5 2  (d)
 

# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert ps to PDF ---
psconvert $ps -Tf -P -A0.1

rm catmerge* gmt.conf gmt.history

