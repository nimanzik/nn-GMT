#!/bin/bash

gmtset PS_MEDIA 15.6cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET -4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

LOCFILE0=../results-16048_183409/single-locations.lonlat
MAJORQUAKES0=../results-16048_183409/single-major-quakes.lonlat
TITLE0="Single-Event Locations"
LOCFILE1=../results-16048_183409/ssst-locations.lonlat
MAJORQUAKES1=../results-16048_183409/ssst-major-quakes.lonlat
TITLE1="Global SSST Locations"
SLAB=../results-16048_183409/slab1.0_sam_top.in


ps=CHILE-SEISMICITY.ps


# #############################################################################
#      MAP VIEW >>> CONFIG                                                    #
# #############################################################################

RMAP=-85/-62/-45/-15
JMAP=M6c

DRY=150 #GRAY70
WET=211 #235 #LIGHTCYAN
B=a5f2.5

Se=c0.1
We=thinnest,GRAY15

# Major Quakes
Sm=a0.5
Wm=thinnest,black

F=+f9p,Helvetica-Bold,BLACK

PROFWIDTH=1
PROFLEFT=-85
PROFRIGHT=-62
PROFLAT=(-22.5 -27.6 -32.75)
PROFLABEL=(A B C)



# --- Initialize ---
psxy -R$RMAP -J$JMAP -T -X0 -Y0 -K > $ps


# #############################################################################
#      MAP VIEW >>> DEPTH COLOR PALLETE                                       #
# #############################################################################
makecpt -Cno_green -T0/600/30 -I > t.cpt


# #############################################################################
#      MAP VIEW >>> PLOT                                                      #
# #############################################################################
XOFFSET=(1.1c  7.4c)
YOFFSET=(12c  0)

for k in 0 1;do

	if [ $k = 0 ];then
    	ANNOT=eWnS+t$TITLE0
	else
    	ANNOT=EwnS+t$TITLE1
	fi

	X=${XOFFSET[$k]}
	Y=${YOFFSET[$k]}
	LOCFILE=`eval "echo $"LOCFILE$k""`
	MAJORQUAKES=`eval "echo $"MAJORQUAKES$k""`

    grdimage -R$R1 -J$J1 ../topo/chile.grd -I../topo/chile.norm  \
	    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
	pscoast -R$RMAP -J$JMAP -S$WET -Di -N1 -Wthinnest -X0 -Y0 -O -K >> $ps
	psbasemap -R -J -L-68/-42/-27/200 -B$B -B"$ANNOT" -O -K --MAP_TICK_LENGTH_PRIMARY=5p/2.5p >> $ps

	# Epicenters
	awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

	# Slab
	awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+r+t -Gblack -O -K >> $ps

    # Major Earthquakes
    #awk '{print $4, $5, $6}' $MAJORQUAKES | psxy -R -J -S$Sm -Ct.cpt -W$Wm -O -K >> $ps

	# Cross Section Rectangles
	for i in "${PROFLAT[@]}";do
	    TOP=`echo "$i - $PROFWIDTH" | bc -l`
	    BOT=`echo "$i + $PROFWIDTH" | bc -l`
    	psxy -R -J -Wthin,BLACK,-- -A -N -O -K >> $ps <<EOF
    	$PROFLEFT  $TOP
    	$PROFRIGHT $TOP
    	$PROFRIGHT $BOT
    	$PROFLEFT  $BOT
    	$PROFLEFT  $TOP

EOF
	done

	# Add profile labels
	n=0
	for i in "${PROFLAT[@]}";do
	    LABEL=${PROFLABEL[$n]}
    	pstext -R -J -F$F+j -N -O -K >> $ps << EOF
    	-85.5  $i  MR  ${LABEL}
    	-61.5  $i  ML  ${LABEL}@+'@+
EOF
		n=`expr $n + 1`
	done
done

# Colorbar
psscale -D-0.7c/1.5/13.4c/0.25h -B:Depth\ \[km\]: -Ct.cpt -Y-2.3c -O -K >> $ps



# #############################################################################
#      CROSS SECTIONS >>> CONFIG                                              #
# #############################################################################
RCROSS=-80/-65/-10/350
JCROSS=X3.91c/-1.4c

Bx=a5f2.5
By=a100f50

Se=+0.08
Ge=GRAY10
We=thinnest,GRAY10


# #############################################################################
#      CROSS SECTIONS >>> PLOT                                                #
# #############################################################################

function plot_profiles {
	X0=$1        # X_origin of first subplot
	Y0=$2        # Y_origin of second subplot
	LOCFILE=$3   # hypocenter locations file
	TITLE=$4     # plot title

	DY=-1.9c

	N=0
	for i in "${PROFLAT[@]}";do
		awk '{print $4, $5, $6}' $LOCFILE | project -C$PROFLEFT/$i -E$PROFRIGHT/$i \
			-W-$PROFWIDTH/$PROFWIDTH -Lw -Fxz > proj$N.dat
		N=`expr $N + 1`
	done

	# Top subplot
	psbasemap -R$RCROSS -J$JCROSS -Bx$Bx -By$By -BeWns+t"$TITLE" -X$X0 -Y$Y0 -O -K \
	    --MAP_TITLE_OFFSET=4p >> $ps
	awk '{print}' proj0.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $ps

    LABEL=${PROFLABEL[0]}
	pstext -R -J -F$F+j -N -O -K >> $ps << EOF
	-64.4  15  TL  ${LABEL}-${LABEL}@+'@+
EOF

	# Bottom subplots
	for i in 1 2;do
	    if [ $i = 2 ];then
            psbasemap -R$RCROSS -J$JCROSS -Bx$Bx+l"Longitude [deg]" -By$By -BeWnS -Y$DY -O -K >> $ps
	    else
            psbasemap -R$RCROSS -J$JCROSS -Bx$Bx -By$By+l"Depth [km]" -BeWns -Y$DY -O -K >> $ps
	    fi

	    awk '{print}' proj$i.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $ps

        LABEL=${PROFLABEL[$i]}
	    pstext -R -J -F$F+j -N -O -K >> $ps << EOF
	    -64.4  15  TL  ${LABEL}-${LABEL}@+'@+
EOF
	done
}

plot_profiles -6.1c -1.9c $LOCFILE0 "$TITLE0"
plot_profiles  7.4c  3.8c $LOCFILE1 "$TITLE1"


# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-8c -Y5.2c -O -K >> $ps << END
-5.2 53.8 (a)
-5.2  1.9 (b)
END


# --- Finalize ---
psxy -R -J -T -O >> $ps

# remove redundant files
rm proj[0-9].dat

# Convert ps file to PDF
psconvert $ps -Tf -P -A0.2


rm t.cpt gmt.conf gmt.history

