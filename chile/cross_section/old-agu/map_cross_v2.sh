#!/bin/bash

e=$1
eC=$2
ps=$3

eS=c0.07
eW=thinnest,GRAY20

sP=./metadata/Pstations_noCX.lonlat
spP=./metadata/pPstations_noCX.lonlat
sS=t0.3
sCP=BLACK
sCpP=BLUE


Rmap=-90.5/-62/-45.5/-5
Jmap=M4i #C-76/-25/5i

Rpro=-90.5/-62/0/650
Jpro=X3i/-1.1i



# ---------------------- PLOT MAP --------------------------------------------

pscoast -R$Rmap -J$Jmap -Gdarkgray -Slightblue -Ba5 -X1.5i -Y1i -K > $ps


awk '{print $4,$5}' $e | psxy -R -J -S$eS -G$eC -W$eW -O -K >> $ps

awk '{print $2, $3}' $sP | psxy -R -J -S$sS -G$sCP -Wthinnest -O -K >> $ps
awk '{print $2, $3}' $spP | psxy -R -J -S$sS -G$sCpP -Wthinnest -O -K >> $ps

for i in `seq -18 -5 -38`;do
    psxy -R -J -Wthick -A -O -K >> $ps << EOF
    -90.5  $i
    -62.0  $i
EOF

    psxy -R -J -Sc0.001 -G0 -O -K -Ey/thick >> $ps <<EOF
    -76.25  $i  1
EOF
done

j=1
for i in `seq -18 -5 -38`;do
    l=`expr $i + 1`
    pstext -R -J -F+f12,31,RED2+j -Gwhite -O -K >> $ps << EOF
    -90.0  $l  BL  $j
    -62.5  $l  BR  $j@+'@+
EOF
j=`expr $j + 1`
done



# ---------------------- PLOT PROFILE (CROSS SECTION) VIEWS ------------------

for i in `seq -18 -5 -38`;do
awk '{print $4,$5,$6}' $e | project -C-90.5/$i -E-62/$i -W-1/1 -Fxz > prof$i.dat
done

psbasemap -R$Rpro -J$Jpro -Bxa5 -Bya200f50+l"Depth [km]" -BWesn -X5.6i -Y5.8i -O -K >> $ps
awk '{print}' prof-18.dat | psxy -R -J -S$eS -G$eC -W$eW -N -O -K >> $ps
pstext -R -J -F+f14,31,RED2+j -N -O -K >> $ps << EOF
-90.5  -50  BL  1
-62.0  -50  BR  1@+'@+
EOF


j=2
for i in `seq -23 -5 -38`;do
    if [ $i = -38 ];then
        psbasemap -R$Rpro -J$Jpro -Bxa5+l"Longitude [deg]" -Bya200f50+l"Depth [km]" -BWeSn -Y-1.5i -O -K >> $ps
    else
        psbasemap -R$Rpro -J$Jpro -Bxa5 -Bya200f50+l"Depth [km]" -BWesn -Y-1.5i -O -K >> $ps
    fi

    awk '{print}' prof$i.dat | psxy -R -J -S$eS -G$eC -Wthinnest -N -O -K >> $ps
    pstext -R -J -F+f14,31,RED2+j -N -O -K >> $ps << EOF
    -90.5  -50  BL  $j
    -62.5  -50  BR  $j@+'@+
EOF
    j=`expr $j + 1`
done


rm prof*.dat
