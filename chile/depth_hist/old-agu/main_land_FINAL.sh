#!/bin/bash

gmtset FONT_TITLE 20p,31
gmtset FONT_ANNOT_PRIMARY 12p,4
gmtset FONT_LABEL 20p,31
gmtset MAP_TITLE_OFFSET 8p

ps=DEPTH_HIST_FINAL.ps

LOCGFN=$1      # geofon
LOCSINGLE=$2   # nlloc
LOCSSST=$3     # ssst


R=0/300/0/25
J=X3i/2i

W=5
Z=1

G=DARKGRAY #85/26/139
L=thick,BLACK #GRAY40

Ytick=a10f5
Ylabel="Frequency"
Xtick=a30f5
Xlabel="Depth [km]"


# ---------------------- histograms ----------------------
awk '{print $6}' $LOCGFN | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+l$Ylabel+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"GEOFON bulletin data" -X2.4 -Y5 -K > $ps

awk '{print $6}' $LOCSINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"Single event locations" -X3.7i -O -K >> $ps

awk '{print $6}' $LOCSSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"SSST locations" -X3.7i -O >> $ps

# --- convert ps file
ps2raster $ps -TG -E600 -P -A0.2

