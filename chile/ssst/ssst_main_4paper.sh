#!/bin/bash

gmtset PS_MEDIA 23.0cx15.6c
gmtset PS_PAGE_ORIENTATION landscape

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

STATIONS=(GE_LVC  GT_LPAZ  US_GOGA)
SSSTDIR=../results-16048_183409/chile-ssst-values
SINGLERESDIR=../results-16048_183409/single-residuals
SSSTRESDIR=../results-16048_183409/ssst-residuals
SLAB=../results-16048_183409/slab1.0_sam_top.in
STACOORD=./metadata/stations.nll


ps=CHILE-SSST-VALUES.ps


# #############################################################################
#      MAP VIEW >>> CONFIG                                                    #
# #############################################################################
RMAP=-85/-62/-45.0/-15.0
JMAP=M4.3c
BMAP=a5f2.5

Ss=t0.45
SsGLOB=t0.25
Gs=TAN2
Ws=thick,BLACK
WsGLOB=thin,BLACK
Se=c0.15
T=-3/3/0.01

DRY=150 #GRAY70
WET=211 #235 #LIGHTCYAN
F=+f8.5p,Helvetica-Bold,BLACK+jTC



# --- INITIALIZE ---
psxy -R$RMAP -J$JMAP -T -K -X0 -Y0> $ps


# #############################################################################
#      MAP VIEW >>> COLOR PALLETE                                             #
# #############################################################################
makecpt -C./cpt/BlRe.cpt -T$T > t.cpt


# #############################################################################
#      MAP VIEW >>> PLOT                                                      #
# #############################################################################
XOFFSET=(1.3c  4.85c  4.85c)
YOFFSET=(14.5c  0.0c  0.0c)

for i in `seq 0 2`;do
    if [ $i = 0 ];then
        ANNOT=eWnS
    else
        ANNOT=ewnS
    fi

    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}

    STA=${STATIONS[$i]}    
    SSSTFILE=${SSSTDIR}/${STA}.P.lonlat

    grdimage -R$R1 -J$J1 ../topo/chile.grd -I../topo/chile.norm  \
	    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
    pscoast -R$RMAP -J$JMAP -S$WET -B$BMAP -B$ANNOT -N1 -Wthinnest -X0 -Y0 -O -K >> $ps
    psbasemap -R -J -L-68/-42/-42/200 -O -K >> $ps

    # SSST values
    awk '{print $1,$2,$5}' $SSSTFILE | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps
    # Colorbar
    psscale -D0.2/1.5/2.5/0.3 -B1f0.5:SSST\ \[s\]: -Ct.cpt -O -K >> $ps

	# Slab
	awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+r+t -Gblack -O -K >> $ps
    
    # Station
    cat $STACOORD | grep $STA | awk '{print $5,$4}' | psxy -R -J -S$Ss -G$Gs -W$Ws -O -K >> $ps
    cat $STACOORD | grep $STA | awk '{print $5+0.5,$4-1, $2}' | pstext -R -J -F$F -O -K >> $ps
    
    if [ $i = 2 ];then
        # Reference Map
        pscoast -Rg -JA-75/-10/2c -Bg45 -Dc -A5000 -G$DRY -Swhite -X0.2c -Y4.3c -O -K  >> $ps
        # Station
        cat $STACOORD | grep $STA | awk '{print $5,$4}' | psxy -R -J -S$SsGLOB -G$Gs -W$WsGLOB -O -K >> $ps
        cat $STACOORD | grep $STA | awk '{print $5+5,$4-10, $2}' | pstext -R -J -F$F -N -O -K >> $ps
        # Rectangle Around Studied Area
        psxy -R -J -Wthick -O -K >> $ps << EOF
        -85 -15
        -62 -15
        -62 -45
        -85 -45
        -85 -15
EOF
    fi
done


# #############################################################################
#      RESIDUALS HISTOGRAM >>> CONFIG                                         #
# #############################################################################
RHIST=-8/8/0/12
JHIST=X4.3c/3c

Wh=0.1
Z=1
Gh=gray30
L=thin,gray30

YTICK=a4f2
XTICK=a2f1

X0T=-7.5
Y0T=11.3
DY=1.4
F=+f8.5p,Helvetica,BLACK+jTL


# #############################################################################
#      SINGLE LOCATION RESIDUALS HISTOGRAM >>> PLOT                           #
# #############################################################################
XOFFSET=(-9.9c  4.85c  4.85c)
YOFFSET=(-8.9c  0  0)

for i in `seq 0 2`;do
    if [ $i = 0 ];then
        ANNOT=eWnS
        Bpy=$YTICK+l"Frequency [%]"
    else
        ANNOT=ewnS
        Bpy=$YTICK
    fi

    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}

    STA=${STATIONS[$i]}
    RESFILE=${SINGLERESDIR}/${STA}.P.txt

    awk 'NR>1 {print $2}' $RESFILE | pshistogram -R$RHIST -J$JHIST -W$Wh -Z$Z -L$L -G$Gh \
        -Bpy"$Bpy" -Bpx$XTICK+l"Time Residual [s]" -B$ANNOT -X$X -Y$Y -O -K >> $ps

    # Add statistical measures text
    echo "$X0T $Y0T Single-event loc." | pstext -R -J \
        -F+f8.5,Helvetica-Bold,BLACK+jTL -O -K >> $ps
    echo "`echo $X0T*-1 | bc -l` $Y0T $STA" | pstext -R -J \
        -F+f8.5,Helvetica-Bold,BLACK+jTR -O -K >> $ps
    echo "$X0T `echo $Y0T-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $RESFILE`" | \
        pstext -R -J -F$F -O -K >> $ps
    echo "$X0T `echo $Y0T-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-4*$DY | bc -l`  @%12%s@%%=$(echo "scale=2; sqrt(`awk 'NR == 1 {print $9}' $RESFILE`)" | bc -l)" | \
        pstext -R -J -F$F -N -O -K >> $ps

done


# #############################################################################
#      SSST LOCATION RESIDUALS HISTOGRAM >>> PLOT                             #
# #############################################################################
XOFFSET=(-9.7c  4.85c  4.85c)
YOFFSET=(-4.6c  0  0)

for i in `seq 0 2`;do
    if [ $i = 0 ];then
        ANNOT=eWnS
        Bpy=$YTICK+l"Frequency [%]"
    else
        ANNOT=ewnS
        Bpy=$YTICK
    fi

    X=${XOFFSET[$i]}
    Y=${YOFFSET[$i]}

    STA=${STATIONS[$i]}
    RESFILE=${SSSTRESDIR}/${STA}.P.txt

    awk 'NR>1 {print $2}' $RESFILE | pshistogram -R$RHIST -J$JHIST -W$Wh -Z$Z -L$L -G$Gh \
        -Bpy"$Bpy" -Bpx$XTICK+l"Time Residual [s]" -B$ANNOT -X$X -Y$Y -O -K >> $ps

    # Add statistical measures text
    echo "$X0T $Y0T Global SSST loc." | pstext -R -J \
        -F+f8.5,Helvetica-Bold,BLACK+jTL -O -K >> $ps
    echo "`echo $X0T*-1 | bc -l` $Y0T $STA" | pstext -R -J \
        -F+f8.5,Helvetica-Bold,BLACK+jTR -O -K >> $ps
    echo "$X0T `echo $Y0T-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $RESFILE`" | \
        pstext -R -J -F$F -O -K >> $ps
    echo "$X0T `echo $Y0T-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $RESFILE`" | \
        pstext -R -J -F$F -N -O -K >> $ps
    echo "$X0T `echo $Y0T-4*$DY | bc -l`  @%12%s@%%=$(echo "scale=2; sqrt(`awk 'NR == 1 {print $9}' $RESFILE`)" | bc -l)" | \
        pstext -R -J -F$F -N -O -K >> $ps
done



# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-10.2c -Y3c -O -K >> $ps << END
-1.0 57.0 (a)
-1.0 22.0 (b)
-1.0  2.0 (c)
END



# --- FINALIZE ---
psxy -R -J -T -O >> $ps


# convert to PDF
psconvert $ps -Tf -P -A0.2

rm t.cpt gmt.conf gmt.history

