# Nsamples: 52  MAD:  0.520  mean: -0.241  variance:  0.690
gfz2010ebhc  -0.23930
gfz2010grjd  -1.37620
gfz2010iull  -0.70680
gfz2010mkuu  -0.33420
gfz2010nmpc  -0.63210
gfz2010wioh   2.77780
gfz2011aatr  -0.55430
gfz2011enva  -0.97400
gfz2011gkzt  -2.03430
gfz2011hlsa  -0.07080
gfz2011lcxq  -0.44690
gfz2011lzwj  -0.57830
gfz2011rewa  -0.60590
gfz2011toub  -0.66830
gfz2011wxfs  -0.86050
gfz2012enno  -0.12810
gfz2012jlox  -0.58400
gfz2012kkup  -0.31800
gfz2012klhx   0.19860
gfz2012kudo  -0.75550
gfz2012ldxg  -0.07000
gfz2012teic   0.02010
gfz2012vrgb   0.20920
gfz2012wypz  -0.51300
gfz2013drxz   0.28760
gfz2013jkut  -0.33970
gfz2013kdeo  -0.87820
gfz2013nwdi  -0.88090
gfz2013svci  -0.19290
gfz2014ande  -0.89550
gfz2014bwue  -0.67690
gfz2014fgvi   0.00770
gfz2014frdz   2.76450
gfz2014ftka  -0.30050
gfz2014fval   0.14810
gfz2014gkgf  -0.79670
gfz2014gkqf   0.22730
gfz2014gmnb  -0.37560
gfz2014goba  -0.18570
gfz2014goqw   0.15530
gfz2014gull  -0.33360
gfz2014jpep   0.20260
gfz2014nqfm  -0.97450
gfz2014oinz  -1.48940
gfz2014qqhm  -0.29030
gfz2014rwos   0.39780
gfz2014ssvz  -0.63610
gfz2014vhld   1.17830
gfz2015cyjd   1.06350
gfz2015emcg   0.70410
gfz2015gckd  -0.49350
gfz2015lhjy  -0.66800
