# Nsamples: 52  MAD:  0.622  mean: -0.282  variance:  0.362
gfz2013nkgj  -0.35420
gfz2013nwdi  -0.96570
gfz2013qmer  -0.80660
gfz2013svci   0.12440
gfz2013uauo   0.22470
gfz2013xsdt  -0.07910
gfz2014afmx   0.20830
gfz2014ande  -0.43230
gfz2014bwue  -0.35440
gfz2014fgvi  -0.50720
gfz2014fhla  -0.75430
gfz2014fkmr  -1.07880
gfz2014ftka   0.29910
gfz2014gkgf  -0.47820
gfz2014gkqf   0.41280
gfz2014gmhr   1.41430
gfz2014gmnb  -0.66560
gfz2014goba  -0.50940
gfz2014gull   0.00000
gfz2014hasj   0.18450
gfz2014hocg   0.17530
gfz2014jjhc  -0.15180
gfz2014jpep  -0.67130
gfz2014lgqn   0.46840
gfz2014lxnc  -1.16690
gfz2014lyhk  -0.73440
gfz2014macx  -0.79360
gfz2014nqfm  -0.22110
gfz2014nrie   0.29310
gfz2014oinz  -0.49440
gfz2014owxc  -1.83990
gfz2014qlxd  -0.48900
gfz2014qqhm  -1.64260
gfz2014ssvz  -0.05240
gfz2014ubwn   0.10030
gfz2015chhn  -0.18120
gfz2015cyjd   0.36450
gfz2015fsjj   0.37970
gfz2015gckd   0.70360
gfz2015kquq  -0.54130
gfz2015kxxs   0.92520
gfz2015lhjy  -0.19910
gfz2015mpte  -0.24190
gfz2015sfdd  -1.01720
gfz2015sfdy  -0.88700
gfz2015sfno  -0.38350
gfz2015sjgk  -0.96230
gfz2015smym  -0.43660
gfz2015snwh  -0.45260
gfz2015soxc   0.19810
gfz2015svwm  -0.10080
gfz2015taml  -0.50390
