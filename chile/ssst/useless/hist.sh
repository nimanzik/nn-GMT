#!/bin/bash

ifile=$1

fname=$(basename "$ifile")
sta="${fname%.*.*}"

ffile=./final_res/$fname

ps=${sta}.ps

Col=DARKGRAY

R=-10/10/0/30
J=X3.6i/1.8i

itxt=+f14,4,MEDIUMBLUE+jTL
ftxt=+f14,4,RED3+jTL


# ----------------------------------------------------------------------------
# histogram of initial residuals
awk 'NR>1{print $2}' $ifile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,0 -G$Col -Bpya10f5+l"Frequency"+u"%" -Bpxa2f1 --FONT_LABEL=18p -BWS -P -X5.5 -Y17 -K > $ps

# add text
echo "-8 30 Initial Locations" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "4 30 N=`awk 'NR==1 {print $3}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "4 25 MAD=`awk 'NR==1 {print $5}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "4 20 @%12%m@%%=`awk 'NR==1 {print $7}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "4 15 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of final residuals
awk 'NR>1{print $2}' $ffile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,0 -G$Col -Bpya10f5+l"Frequency"+u"%" -Bpxa2f1+l"Travel-time residual [s]" --FONT_LABEL=18p -BWS -Y-2.6i -K -O >> $ps

# add text
echo "-8 30 Final Locations" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "4 30 N=`awk 'NR==1 {print $3}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "4 25 MAD=`awk 'NR==1 {print $5}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "4 20 @%12%m@%%=`awk 'NR==1 {print $7}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "4 15 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ffile`" | pstext -R -J -F$ftxt -N -O >> $ps

# convert to PNG
ps2raster $ps -TG -P -A0.2

mv *.png figures/png/
mv $ps ./figures/ps/

