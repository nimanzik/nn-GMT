# Nsamples: 25  MAD:  0.463  mean:  0.098  variance:  0.201
gfz2010bxxs   0.52061
gfz2010ebhc  -0.04620
gfz2010grjd  -0.68307
gfz2010nmpc  -0.40712
gfz2011aatr   0.39608
gfz2011enva  -0.14851
gfz2011hlqw   0.36338
gfz2011hlsa   0.14835
gfz2011toub  -0.02194
gfz2012enno  -0.45415
gfz2012klhx   0.37165
gfz2012ldxg   0.25588
gfz2012vrgb  -0.25410
gfz2013jkut   0.61579
gfz2014bwue  -0.23098
gfz2014gkqf   0.08126
gfz2014goba  -0.09002
gfz2014hasj   0.16149
gfz2014jpep  -0.71556
gfz2014lxnc   0.21748
gfz2014nqfm  -0.23594
gfz2014qlxd  -0.10048
gfz2014qqhm   0.76353
gfz2014ssvz   0.94148
gfz2015mpte   0.99555
