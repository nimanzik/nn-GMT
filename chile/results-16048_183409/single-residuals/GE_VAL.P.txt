# Nsamples: 40  MAD:  0.642  mean: -0.447  variance:  1.111
gfz2010ahqi  -1.35760
gfz2010eaoy   1.81390
gfz2010excp  -0.50239
gfz2010iull  -0.16311
gfz2011aatr  -1.29460
gfz2011enva  -0.11798
gfz2011jplh  -0.81319
gfz2011lcxq   0.20219
gfz2011lzwj  -0.73914
gfz2011rewa  -1.12080
gfz2011wxfs  -0.34924
gfz2012jlox  -0.55843
gfz2012kkup  -0.50826
gfz2012kudo  -0.99532
gfz2012sphd  -1.15050
gfz2012wypz  -0.78654
gfz2013drxz  -1.02900
gfz2013kdeo   0.49946
gfz2013nkgj  -1.47660
gfz2013nwdi  -0.64451
gfz2013svci  -0.95092
gfz2013vjjc   3.12230
gfz2014dgiy  -2.55350
gfz2014fgvi  -0.78177
gfz2014fhla  -0.39141
gfz2014ftka   0.05438
gfz2014gkgf  -0.26193
gfz2014gmhr   1.34820
gfz2014goba   0.76251
gfz2014gzue  -0.76642
gfz2014ssvz  -0.44106
gfz2015chhn  -2.60810
gfz2015cyjd  -0.22559
gfz2015fjhk  -0.68427
gfz2015fsjj  -0.20492
gfz2015lhjy  -1.02480
gfz2015lytv   0.11273
gfz2015sfdd  -1.26240
gfz2015svwm  -1.43880
gfz2015taml   1.42190
