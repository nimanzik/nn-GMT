# Nsamples: 33  MAD:  0.381  mean:  0.030  variance:  0.309
gfz2010chnv  -1.66430
gfz2010ebhc  -0.04827
gfz2010iull  -0.14501
gfz2010nmpc  -0.18322
gfz2010nwef   0.12784
gfz2010usho   0.48171
gfz2011aatr  -0.17025
gfz2011enva   0.33686
gfz2011gkzt   0.05772
gfz2011jplh   0.28475
gfz2011lcxq  -0.10222
gfz2011lzwj  -0.35056
gfz2011rewa  -0.61323
gfz2011toub   0.02767
gfz2011wxfs  -0.18658
gfz2012enno  -0.61180
gfz2012jlox  -0.39216
gfz2012kkup   0.20278
gfz2012kuct   0.06022
gfz2012kudo   0.34841
gfz2012ldxg  -0.58797
gfz2014gkgf   0.14301
gfz2014gkqf   0.53808
gfz2014lgqn   0.25900
gfz2014qqhm  -0.43242
gfz2014ssvz   1.27560
gfz2015cyjd  -0.47624
gfz2015fjhk  -0.10859
gfz2015kxxs   0.22420
gfz2015lhjy   0.83760
gfz2015mpte  -0.19116
gfz2015sfdd   1.18250
gfz2015taml   0.85343
