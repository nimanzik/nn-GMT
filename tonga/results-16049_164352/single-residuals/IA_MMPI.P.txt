# Nsamples: 49  MAD:  0.827  mean:  0.601  std:  1.165
gfz2010crgy   0.19179
gfz2010cyid  -1.12820
gfz2010fjme   0.63249
gfz2010fkpa   2.91070
gfz2010habf   0.49025
gfz2010jlvp   0.88273
gfz2010uhjy  -0.09910
gfz2010xpzq   1.22270
gfz2010yfdr   0.60129
gfz2010zjun   0.45362
gfz2011ahvh   1.15660
gfz2011awsv   0.04550
gfz2011bpqm   1.09970
gfz2011bsvr   0.29345
gfz2011dabj   1.11700
gfz2011gaax   0.81967
gfz2011gkdx  -3.13970
gfz2011gnbk   1.01310
gfz2011gpyr   2.11190
gfz2011uowi   0.21928
gfz2011uqry   0.05816
gfz2011wpzt   0.37483
gfz2012cvfr   1.63860
gfz2012ekfp   0.02051
gfz2012elek   1.40910
gfz2012gnow   1.36540
gfz2012sxbo  -2.87920
gfz2012uvoa   0.55470
gfz2012uzww   0.93414
gfz2012wgmg   1.22430
gfz2012zhyz   0.92704
gfz2013byyy   0.06119
gfz2013cyvx   1.31170
gfz2013dftb   1.26030
gfz2013iuyv   1.06140
gfz2013oizq   3.92860
gfz2013phhi   1.64140
gfz2013qigb  -0.28019
gfz2013qoak  -1.07380
gfz2013utqa   0.63620
gfz2013uwjo   1.40380
gfz2013vmrv   0.85692
gfz2013wyfl   0.72389
gfz2013xahp   0.64893
gfz2013xswc   0.66635
gfz2013zkzb  -0.67970
gfz2013zlak  -0.22306
gfz2014dece  -0.45093
gfz2014jayh   1.45450
