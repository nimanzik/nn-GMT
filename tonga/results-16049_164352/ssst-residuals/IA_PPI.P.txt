# Nsamples: 94  MAD:  1.069  mean: -0.041  std:  0.958
gfz2010aggj  -0.70833
gfz2010bklj   0.48680
gfz2010btme   1.81480
gfz2010ngge   0.06424
gfz2010pclq   2.56680
gfz2010qdqk   0.76315
gfz2010qrta   1.20560
gfz2010rkuu   1.46930
gfz2010rljv   0.17345
gfz2010rnwt   2.21060
gfz2010tmba   0.86981
gfz2010ugws  -0.41513
gfz2010uhjy   0.20346
gfz2010vong  -0.00000
gfz2010wakc  -1.22290
gfz2010xnbn  -0.56660
gfz2010xpzq   0.08795
gfz2011ahvh  -0.74418
gfz2011bcvc  -0.43070
gfz2011bffb   1.25290
gfz2011bpqm  -0.72208
gfz2011bsvr  -1.42570
gfz2011bygs   0.10710
gfz2011bygu  -1.14740
gfz2011cdge  -0.16371
gfz2011dabj  -1.25940
gfz2011dpyu  -1.00860
gfz2011exxq   0.89667
gfz2011fnie   0.16040
gfz2011fszl   0.79735
gfz2011fzcw  -0.50443
gfz2011gpyr  -0.21057
gfz2011hqjs  -1.23100
gfz2011jryv  -0.13250
gfz2011kjzt  -1.27390
gfz2011kjzv  -2.24870
gfz2011lkkf   1.51780
gfz2011lwmg   0.45596
gfz2011pfwq  -0.28325
gfz2011ppsc   0.31537
gfz2011qenl   1.09300
gfz2011qukj   0.46737
gfz2011rkau  -0.52661
gfz2011rnny  -0.20739
gfz2011sdax  -1.00860
gfz2011sqco  -0.74978
gfz2011sxno   0.36574
gfz2012uvoa   0.17556
gfz2012uzww   0.25054
gfz2012wgmg   0.92881
gfz2012wkki   1.04910
gfz2012xncm   0.89252
gfz2012xxdj  -0.28216
gfz2013agmt   1.82180
gfz2013akkd   0.89486
gfz2013bwgw   0.06850
gfz2013byyy  -0.32750
gfz2013cvve   0.72157
gfz2013cyvx  -0.79078
gfz2013delq  -0.82178
gfz2013dftb   0.70836
gfz2013dngd   0.45713
gfz2013dzvr   0.54255
gfz2013efct   0.98225
gfz2013fnpa   0.71307
gfz2013fulo  -0.97523
gfz2013fvht  -0.66255
gfz2013fvyr  -0.07519
gfz2013icql  -1.11850
gfz2013iuyv  -1.24070
gfz2013yqtu   1.80720
gfz2013yxhf   0.53732
gfz2013zfbz  -0.58283
gfz2013ziix  -1.19520
gfz2014eirs   0.03443
gfz2014eoay   0.59687
gfz2014epfm   0.65143
gfz2014flxk  -0.57477
gfz2014fsir  -1.27720
gfz2014fsje  -1.16790
gfz2014fxte  -0.48932
gfz2014gnbw  -0.04956
gfz2014gypw   0.84738
gfz2014hkcw  -0.13377
gfz2014hqhv   0.68527
gfz2014hzzf  -1.48490
gfz2014igrp   0.06433
gfz2014iltl  -1.13190
gfz2014irlf  -1.66990
gfz2014irln  -1.63790
gfz2014irly  -1.23070
gfz2014iusw  -0.90748
gfz2014jarg  -0.39352
gfz2014jayh  -0.19531
