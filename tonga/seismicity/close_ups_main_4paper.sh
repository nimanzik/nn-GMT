#!/bin/bash

gmtset PS_MEDIA 7.2cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET -8p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

LOCFILE0=../results-16049_164352/single-locations.lonlat
TITLE0="Single-Event Locations"
LOCFILE1=../results-16049_164352/ssst-locations.lonlat
TITLE1="Global SSST Locations"

SLAB=../results-16049_164352/slab1.0_ker_top.in


ps=TONGA-CLOSE-UPS.ps


# #############################################################################
#          MAP VIEW >>> CONFIG                                                #
# #############################################################################

R1=-174.0/-16.2/-171.5/-14.0r
J1=M2c

R2=-175.0/-21.5/-172.5/-19.4r
J2=M2c

R3=+180.0/-22.5/-176.5/-19.4r
J3=M3c

R4=+178.0/-26.0/-178.3/-22.5r

DRY=150 #gray70
WET=235 #lightcyan

Se=c0.11
We=thinnest,gray5

F=+f10p,Helvetica-Bold,BLACK


# Rectangle Labels
cat > rectangle_labels.txt << EOF
-171.6  -14.2  TR  1
-172.6  -19.6  TR  2
+180.1  -19.5  TL  3
+178.1  -22.6  TL  4
EOF


# #############################################################################

# --- Initialize ---
psxy -R$R1 -J$J1 -T -X0 -Y0 -K > $ps


# #############################################################################
#           MAP VIEW >>> DEPTH COLOR PALLETE                                  #
# #############################################################################
makecpt -Cno_green -T0/600/30 -I > t.cpt


# #############################################################################
#           MAP VIEW >>> PLOT                                                 #
# #############################################################################


function mainplot {
    X0=$1
    Y0=$2
    LOCFILE=$3
    TITLE=$4
    # ##### Subplot (1) #####
    pscoast -R$R1 -J$J1 -G$DRY -Di -N1 -Wthinnest -X$X0 -Y$Y0 -O -K >> $ps
    psbasemap -R -J -B1000 -Bewns+t"$TITLE" -O -K >> $ps

    # Slab
    awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -O -K >> $ps

    # Epicenters
    awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

    # Rectangle Labels
    pstext -R -J -F$F+j -O -K rectangle_labels.txt >> $ps



    # ##### Subplot (2) #####
    pscoast -R$R2 -J$J2 -G$DRY -Di -N1 -Wthinnest -X0 -Y-2.2c -O -K >> $ps
    psbasemap -R -J -B1000 -Bewns -O -K >> $ps

    # Slab
    awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -O -K >> $ps

    # Epicenters
    awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

    # Rectangle Labels
    pstext -R -J -F$F+j -O -K rectangle_labels.txt >> $ps



    # ##### Subplot (3) #####
    pscoast -R$R3 -J$J3 -G$DRY -Di -N1 -Wthinnest -X0 -Y-3.2c -O -K >> $ps
    psbasemap -R -J -B1000 -Bewns -O -K >> $ps

    # Slab
    awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -O -K >> $ps

    # Epicenters
    awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

    # Rectangle Labels
    pstext -R -J -F$F+j -O -K rectangle_labels.txt >> $ps


    # ##### Subplot (4) #####
    pscoast -R$R4 -J$J4 -G$DRY -Di -N1 -Wthinnest -X0 -Y-3.5c -O -K >> $ps
    psbasemap -R -J -B1000 -Bewns -O -K >> $ps

    # Slab
    awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -O -K >> $ps

    # Epicenters
    awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

    # Rectangle Labels
    pstext -R -J -F$F+j -O -K rectangle_labels.txt >> $ps
}

mainplot  0.3c  12.0c  $LOCFILE0  "SE Loc."
mainplot  3.4c   8.9c  $LOCFILE1  "SSST Loc."


# #############################################################################
#           MAP VIEW >>> COLOR BAR                                            #
# #############################################################################
psscale -Dx-3.4c/1.5+w6.4c/0.25+jTL+h+ef -Ba120f30:Depth\ \[km\]: -Ct.cpt -Y-1.8c -O -K >> $ps


# #############################################################################


# --- Finalize ---
psxy -R -J -T -O >> $ps


# Convert ps file to PDF
psconvert $ps -Tf -P -A0.2


rm gmt.conf gmt.history

