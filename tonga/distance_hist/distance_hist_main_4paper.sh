#!/bin/bash

gmtset PS_MEDIA 7.2cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/1.5p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

INFILE=./tonga-distance-azimuth.dat

ps=TONGA-EPI-DISTANCES.ps


# #############################################################################
#          HISTOGRAM >>> CONFIG                                               #
# #############################################################################

R=0/100/0/7000
J=X5c/5c

Bx=a25f5+l"Epicentral Distance [deg]"
By=a1500f500+l"Number of Phases"
ANNOT=eWnS

W=2.5
Z=0
G=150 #dodgerblue4
L=thin,25 #deepskyblue4


# #############################################################################
#          HISTOGRAM >>> CONFIG                                               #
# #############################################################################
# --- Initialize ---
psxy -R$R -J$J -T -X0 -Y0 -K > $ps


awk '{print $1}' $INFILE | pshistogram -R$R -J$J -W$W -Z$Z -G$G -L$L \
	-Bx"$Bx" -By"$By" -B$ANNOT -X1.6c -Y1.2c -O -K >> $ps


# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert PS to PDF ---
psconvert $ps -Tf -P -A0.2


rm gmt.conf gmt.history

