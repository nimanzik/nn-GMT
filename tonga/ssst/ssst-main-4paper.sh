#!/bin/bash

gmtset PS_MEDIA 15.6cx9.25c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

STATION=AU_NIUE
SSSTDIR=../results-16049_164352/tonga-ssst-values
SINGLERESDIR=../results-16049_164352/single-residuals
SSSTRESDIR=../results-16049_164352/ssst-residuals

SLAB=../results-16049_164352/slab1.0_ker_top.in
STACOORD=./metadata/stations.nll


ps=TONGA-SSST-AU-NIUE.ps


# #############################################################################
#      MAP VIEW >>> CONFIG                                                    #
# #############################################################################
R1=176.5/-30.1/-168.25/-14r
J1=M4.5c
B1=a5f2.5

R2=-10/700/-30.1/-14
J2=X1.9c/5.1c
Bx2=a300f50+l"Depth [km]"
By2=a5f2.5+u"\\232"

R3=176.5/191.75/-10/700
J3=X4.5c/-1.9c
cat > Xannotes1.txt << EOF
175  a  175\\232
180  a  180\\232
185  a -175\\232
190  a -170\\232
195  a -165\\232
EOF
Bx3=Xannotes1.txt
By3=a300f50+l"Depth [km]"

SHIFT=0.25


# Station
Ss=t0.45
Gs=TAN2
Ws=thick,BLACK

# Hypocenters
Se=c0.1

# Color bar
T=-7/7/0.1


DRY=150 #GRAY70
WET=220 #235 #LIGHTCYAN

F=+f7p,Helvetica-Bold,BLACK+jTC


# #############################################################################
# --- INITIALIZE ---
psxy -R$R1 -J$J1 -T -K -X0 -Y0> $ps


# #############################################################################
#      MAP VIEW >>> COLOR PALLETE                                             #
# #############################################################################
makecpt -C./cpt/BlRe.cpt -T$T > t.cpt


# #############################################################################
#      MAP VIEW >>> PLOT                                                      #
# #############################################################################

STA=${STATION}    
SSSTFILE=${SSSTDIR}/${STA}.P.lonlat

Xdummy=$(echo "4.5+$SHIFT" | bc -l)c
Ydummy=$(echo "1.9+$SHIFT" | bc -l)c
XOFFSETa=(1.4c  $Xdummy  -$Xdummy)
YOFFSETa=(3.1c  0  -$Ydummy)

# ########## Lat-Lon Map ##########
X=${XOFFSETa[0]}
Y=${YOFFSETa[0]}

grdimage -R$R1 -J$J1 ../topo/tonga_west.grd -I../topo/tonga_west.norm \
    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
pscoast -R$R1 -J$J1 -S$WET -B$B1 -BeWNs -N1 -Wthinnest -X0 -Y0 -O -K >> $ps
psbasemap -R -J -L-170.6/-28.2/-22/200 -O -K >> $ps

# SSST values
awk '{print $1, $2, $5}' $SSSTFILE | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps

# Slab
awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -A -O -K >> $ps
pstext -R -J -F+f7p,Helvetica-Bold,BLACK+a73+jTL -O -K >> $ps << EOF
-173.8 -22.8 Tonga Trench
EOF
pstext -R -J -M -F+f7p,Helvetica-Bold,BLACK+a73+jTL -O -K >> $ps << EOF
> -177.0 -29.3 8p 1c c
Kermadec Trench
>
EOF
    
# Station
cat $STACOORD | grep $STA | awk '{print $5,$4}' | psxy -R -J -S$Ss -G$Gs -W$Ws -O -K >> $ps
cat $STACOORD | grep $STA | awk '{print $5-0.3,$4-0.8, $2}' | pstext -R -J -F$F -O -K >> $ps


# ########## Depth-Lat Map ##########
X=${XOFFSETa[1]}
Y=${YOFFSETa[1]}

psbasemap -R$R2 -J$J2 -Bx"$Bx2" -By"$By2" -BENs -X$X -Y$Y -O -K >> $ps
psbasemap -R$R2 -J$J2 -By1000 -Bw -O -K >> $ps

# SSST values
awk '{print $3, $2, $5}' $SSSTFILE | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps


# ########## Lon-Depth Map ##########
X=${XOFFSETa[2]}
Y=${YOFFSETa[2]}

psbasemap -R$R3 -J$J3 -Bxc$Bx3 -By"$By3" -BeWnS -X$X -Y$Y -O -K >> $ps

# SSST values
awk '{
if ($1 < 0)
	print $1+360,$3,$5
else
	print $1,$3,$5
}' $SSSTFILE | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps

# Colorbar
psscale -D4.75c/0.95c/1.9c/0.25 -Ba3f1:SSST\ \[s\]: -Ct.cpt -O -K >> $ps



# #############################################################################
#      HISTOGRAMS >>> CONFIG                                                  #
# #############################################################################
RHIST=-8/8/0/12
JHIST=X4c/3c

Wh=0.1
Z=1
Gh=gray30
L=thin,gray30

By=a4f2+l"Frequency [%]"
Bx=a2f1+l"Time Residual [s]"
ANNOT=eWnS

X0T=-7.7
Y0T=11.3
DY=1.4
F=+f8p,Helvetica,BLACK+jTL


# #############################################################################
XOFFSETb=(9.2c  0)
YOFFSETb=(0  4.25c)

# ########## SSST Residuals ##########
RESFILE=${SSSTRESDIR}/${STA}.P.txt
i=0
X=${XOFFSETb[$i]}
Y=${YOFFSETb[$i]}
awk 'NR>1 {print $2}' $RESFILE | pshistogram -R$RHIST -J$JHIST -W$Wh -Z$Z -L$L -G$Gh \
    -By"$By" -Bx"$Bx" -B$ANNOT -X$X -Y$Y -O -K >> $ps

# Add statistical measures text
echo "$X0T $Y0T Global SSST loc." | pstext -R -J \
    -F+f8,Helvetica-Bold,BLACK+jTL -O -K >> $ps
echo "`echo $X0T*-1 | bc -l` $Y0T $STA" | pstext -R -J \
    -F+f8.5,Helvetica-Bold,BLACK+jTR -O -K >> $ps
echo "$X0T `echo $Y0T-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $RESFILE`" | \
    pstext -R -J -F$F -O -K >> $ps
echo "$X0T `echo $Y0T-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $RESFILE`" | \
    pstext -R -J -F$F -N -O -K >> $ps
echo "$X0T `echo $Y0T-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $RESFILE`" | \
    pstext -R -J -F$F -N -O -K >> $ps
echo "$X0T `echo $Y0T-4*$DY | bc -l`  @%12%s@%%=$(echo "scale=2; sqrt(`awk 'NR == 1 {print $9}' $RESFILE`)" | bc -l)" | \
    pstext -R -J -F$F -N -O -K >> $ps

# ########## Single Residuals ##########
RESFILE=${SINGLERESDIR}/${STA}.P.txt
i=1
X=${XOFFSETb[$i]}
Y=${YOFFSETb[$i]}
awk 'NR>1 {print $2}' $RESFILE | pshistogram -R$RHIST -J$JHIST -W$Wh -Z$Z -L$L -G$Gh \
    -By"$By" -Bx"$Bx" -B$ANNOT -X$X -Y$Y -O -K >> $ps

# Add statistical measures text
echo "$X0T $Y0T Single-event loc." | pstext -R -J \
    -F+f8,Helvetica-Bold,BLACK+jTL -O -K >> $ps
echo "`echo $X0T*-1 | bc -l` $Y0T $STA" | pstext -R -J \
    -F+f8.5,Helvetica-Bold,BLACK+jTR -O -K >> $ps
echo "$X0T `echo $Y0T-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $RESFILE`" | \
    pstext -R -J -F$F -O -K >> $ps
echo "$X0T `echo $Y0T-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $RESFILE`" | \
    pstext -R -J -F$F -N -O -K >> $ps
echo "$X0T `echo $Y0T-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $RESFILE`" | \
    pstext -R -J -F$F -N -O -K >> $ps
echo "$X0T `echo $Y0T-4*$DY | bc -l`  @%12%s@%%=$(echo "scale=2; sqrt(`awk 'NR == 1 {print $9}' $RESFILE`)" | bc -l)" | \
    pstext -R -J -F$F -N -O -K >> $ps



# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-9.2c -Y3c -O -K >> $ps << END
-3.5 2 (c)
32.5 2 (d)
END



# --- FINALIZE ---
psxy -R -J -T -O >> $ps

# convert to PDF
psconvert $ps -Tf -P

rm *annotes*.txt t.cpt gmt.conf gmt.history

